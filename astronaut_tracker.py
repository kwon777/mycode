#!/usr/bin/env python3

import requests
import re

def main():
    url = "http://api.open-notify.org/astros.json"
    trackerRest = requests.get(url)
    trackerJson = trackerRest.json()
    print(trackerJson)


if __name__ == "__main__":
    main()