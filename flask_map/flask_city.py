import folium
import geopandas
import geopy

from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    start_coords = (40.284, -76.649)
    folium_map = folium.Map(location=start_coords, zoom_start=14)
    return folium_map._repr_html_()

@app.route('/sdsu')
def sdsu():
    SDSU = [32.77548308737991, -117.07034929162842]
    circle_map = folium.Map(location=SDSU, zoom_start=13)
    folium.Circle(
        radius=50,
        location=SDSU,
        popup="Target",
        color="Black",
        fill=False,
    ).add_to(circle_map)

    folium.CircleMarker(
        location=SDSU,
        radius=10,
        popup="Home of Aztecs",
        color="Red",
        fill = True
        #fill=True,
        #fill_color="#3186cc"
    ).add_to(circle_map)
    return circle_map._repr_html_()

# Accept a (GET) at this location
# /city?name=Seattle
@app.route("/city", methods = ["GET"])   # other methods could be included in this list
def city():
    if request.method == "GET":
        #print("GET: ", request.args)
        if request.args.get("name"):
            # if user supplied city name
            city = request.args.get("name")
        else:        
            #default city
            city = 'San Diego, CA'
        # print
        print('city: ', city)
        # convert to geo location
        loc = geopy.Nominatim(user_agent='myGeocoder')
        loc = loc.geocode(city)
        #print loc
        print('loc: ', loc)
    
    # coordinates
    coords = [loc.latitude, loc.longitude]
    
    # create the map
    folium_map = folium.Map(location=coords, zoom_start=14)
    
    folium.Circle(
        radius=50,
        location=coords,
        popup=city,
        color="Black",
        fill=False,
    ).add_to(folium_map)

    folium.CircleMarker(
        location=coords,
        radius=10,
        popup=loc.address,
        color="Red",
        fill = True
    ).add_to(folium_map)
    
    # return the HTML code that is the map
    return folium_map._repr_html_()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=2224, debug=True)
