from flask import Flask

import folium

app = Flask(__name__)

@app.route('/')
def index():
    start_coords = (40.284, -76.649)
    folium_map = folium.Map(location=start_coords, zoom_start=14)
    return folium_map._repr_html_()

@app.route('/sdsu')
def sdsu():
    SDSU = [32.77548308737991, -117.07034929162842]
    circle_map = folium.Map(location=SDSU, zoom_start=13)
    folium.Circle(
        radius=50,
        location=SDSU,
        popup="Target",
        color="Black",
        fill=False,
    ).add_to(circle_map)

    folium.CircleMarker(
        location=SDSU,
        radius=10,
        popup="Home of Aztecs",
        color="Red",
        fill = True
        #fill=True,
        #fill_color="#3186cc"
    ).add_to(circle_map)
    return circle_map._repr_html_()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=2224, debug=True)

