#!/usr/bin/env python3
"""
RZFeeser || Alta3 Research
Using regular expression to parse HTTPS responses
"""

import requests
import re

def main():
    # prompt user for "url" and "searchFor"
    print(f"Welcome to the simple HTTP response parser. Where should we search (ex: https://alta3.com)?")
    url = input()
    while True:
        print(f"Great! So we'll try to open this url {url} to search for the phrase: (Type 'qqqq' to exit) ")
        searchFor = input()
        if(searchFor=='qqqq'):
            break
        # send an HTTP GET to th "url", then strip off the attached HTML data
        searchMe = requests.get(url).text
        matches = re.findall(searchFor, searchMe)
        if (matches):
            match_count = len(matches)
            print("Found # of match: ", match_count)
        else:
            print("No match!")

if __name__ == "__main__":
    main()

