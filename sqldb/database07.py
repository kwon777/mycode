import sqlite3

conn = sqlite3.connect('test.db')
print("Opened database successfully")

# get cursor
cursor = conn.cursor()

# insert new 
cursor.execute("INSERT INTO COMPANY (ID,NAME,AGE, ADDRESS,SALARY) VALUES (5, 'Dan', 21, 'Washington', 1000000.00 ) ON CONFLICT DO NOTHING")
print("Insert new entry was successful")

# commit
conn.commit()
print("Commit operation done successfully")

# display table
cursor.execute("SELECT * FROM COMPANY")

#selects = cursor.fetchall()

for row in cursor:
    print("ID = ", row[0])
    print("NAME = ", row[1])
    print("ADDRESS = ", row[3])
    print("SALARY = ", row[4], "\n")

#close connection
conn.close()


